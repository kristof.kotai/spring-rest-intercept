package com.example.demo.users;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

// THIS IS NOT NEEDED, IF User CLASS HAS @COMPONENT

//@Configuration
//public class UserConfiguration
//{
//    @Bean
//    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
//    public User getUser()
//    {
//        User user = new User();
//        return user;
//    }
//}
