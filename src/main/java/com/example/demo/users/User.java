package com.example.demo.users;

import org.springframework.stereotype.Component;

@Component
public class User {

    private String user;
    private String pass;

    public User() {
        System.out.println("USER CREATED!");
    }

    public void status() {
        System.out.println("STATUS: " + user + ":" + pass);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
