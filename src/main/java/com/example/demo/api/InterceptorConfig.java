package com.example.demo.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InterceptorConfig {
    @Bean
    public AuthInterceptor getAuthInterceptor()
    {
        return new AuthInterceptor();
    }
}
