package com.example.demo.api;

import com.example.demo.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private User user;

    @Override
    public boolean preHandle(HttpServletRequest requestServlet, HttpServletResponse responseServlet, Object handler) throws Exception
    {
        System.out.println("INTERCEPTOR PREHANDLE CALLED");

        String authHeader = requestServlet.getHeader("Authorization");
        if (authHeader == null) {
            responseServlet.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            responseServlet.getWriter().write("NOPE!");
            responseServlet.getWriter().flush();
            return false;
        }

        String[] userPassPair = authHeader
            .replaceAll("Bearer ", "")
            .split("=");

        System.out.println("headers: User: " + userPassPair[0] + " Pass: " + userPassPair[1]);

        user.status();
        user.setUser(userPassPair[0]);
        user.setPass(userPassPair[1]);
        System.out.println("after setting:");
        user.status();

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
        System.out.println("INTERCEPTOR POSTHANDLE CALLED");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception
    {
        System.out.println("INTERCEPTOR AFTERCOMPLETION CALLED");
        System.out.println("HandlerInterceptorAdapter: Goodbye " + user.getUser() + "!");
    }
}