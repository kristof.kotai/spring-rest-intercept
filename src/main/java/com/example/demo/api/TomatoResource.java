package com.example.demo.api;

import com.example.demo.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TomatoResource {

    @Autowired
    private User user;

    @Autowired
    private HttpServletRequest request;

    @GetMapping(
        path="/tomato/{id}",
        consumes=MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces=MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<String> tomato(
        @PathVariable int id
    ) throws Exception {
        System.out.println("TomatoResource CALLED");
        user.status();

        Thread.sleep(2000);

        return new ResponseEntity<String>(
            String.format("Hello user ID: %s", id),
            HttpStatus.OK
        );
    }
}
