Example:

1. Start the app
2. Open 2x terminals and make these requests in quick succession:
```
curl -v http://localhost:8080/tomato/2 --header "Content-Type: application/json" --header "Authorization: AAA=111"
curl -v http://localhost:8080/tomato/2 --header "Content-Type: application/json" --header "Authorization: BBB=222"
```
3. You should see a similar output:
```
INTERCEPTOR PREHANDLE CALLED
headers: User: AAA Pass: 111
USER CREATED!
STATUS: null:null
after setting:
STATUS: AAA:111
TomatoResource CALLED
STATUS: AAA:111
INTERCEPTOR PREHANDLE CALLED
headers: User: BBB Pass: 222
USER CREATED!
STATUS: null:null
after setting:
STATUS: BBB:222
TomatoResource CALLED
STATUS: BBB:222
INTERCEPTOR POSTHANDLE CALLED
INTERCEPTOR AFTERCOMPLETION CALLED
HandlerInterceptorAdapter: Goodbye AAA!
INTERCEPTOR POSTHANDLE CALLED
INTERCEPTOR AFTERCOMPLETION CALLED
HandlerInterceptorAdapter: Goodbye BBB!
```